first_name = raw_input("Enter your first name: \n")
last_name = raw_input("Enter your last name: \n")

capital_first_name = first_name.capitalize()
capital_last_name = last_name.capitalize()

print first_name, last_name
print capital_first_name, capital_last_name
print first_name.upper(), last_name.upper()

int_value = 10
print int_value
int_value = 15.20
print int_value
int_value = "Hello String"
print int_value
int_value = 'Hello String in single quote ' +\
    'second line ' +\
    'third line'
print int_value
int_value = """
Hello string in the first line 
this is the second line 
this is the last line
"""
print int_value

# " "
# ' '
# """ """




